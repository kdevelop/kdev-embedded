/*
 * This file is part of KDevelop project
 * Copyright 2016 Patrick José Pereira <patrickelectric@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#pragma once

#include <interfaces/iplugin.h>

#include <QVariant>
#include <QLoggingCategory>
#include <QScopedPointer>

Q_DECLARE_LOGGING_CATEGORY(PLUGIN_EMBEDDED)

class Embedded : public KDevelop::IPlugin
{
    Q_OBJECT

public:
    explicit Embedded(QObject *parent, const QVariantList & = QVariantList());
    ~Embedded() override;

    void firstTimeWizardEvent();
    void boardSettingsEvent();
};
